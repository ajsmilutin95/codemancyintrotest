CREATE DATABASE task3;

USE task3;

CREATE TABLE Student (
	StudentID INT NOT NULL IDENTITY,
	FirstName VARCHAR(50) NOT NULL,
	LastName VARCHAR(50) NOT NULL,
	PRIMARY KEY(StudentID)
);


CREATE TABLE Exam (
	StudentID INT NOT NULL,
	Name VARCHAR(50) NOT NULL,
	Points INT NOT NULL,
	PRIMARY KEY(StudentID,Name),
	FOREIGN KEY (StudentID) REFERENCES Student(StudentID)
);

 


INSERT INTO Student (FirstName,LastName) VALUES
('Petar','Petrovic'),
('Jovan','Jovanovic'),
('Rajko','Rajkovic'),
('Milan','Milanovic'),
('Stasa','Milanovic'),
('Lazar','Jovanovic'),
('Danilo','Sarovic'),
('Miodrag','Mikic'),
('Ana','Stevanovic'),
('Marija','Antic');


INSERT INTO Exam(StudentID,Name,Points) VALUES
(1,'Uvod U Programiranje',100),
(2,'Uvod U Programiranje',32),
(3,'Uvod U Programiranje',68),
(6,'Uvod U Programiranje',51),
(7,'Matematicka Analiza 1',83),
(7,'Matematicka Anaiza 2',97),
(4,'Baze podataka',100),
(5,'Baze podataka',94),
(6,'Baze podataka',60),
(7,'Baze podataka',100);




SELECT s.FirstName,s.LastName,e.Points FROM Student s, Exam e
WHERE s.StudentID=e.StudentID AND  e.Name='Baze Podataka';


SELECT e.Name, e.Points from Student s , Exam e
WHERE e.StudentID=s.StudentID AND s.StudentID=7;
