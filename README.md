# README #


### Task 1 ###

* Has both recursive and non recursive functions
* Works with command line arguments
* Just run .exe or  build it from visual studio
### Task 2 ###

* Windows Forms application
* Contains Post.cs with members and methods needed for this problem
* RandomDate.cs is for automated generation dates of test data
* There are 3 buttons to use, one for adding a costum number of posts, one for sorting by date, and one for search by ID
* Ids are from 1 to n, n being the number of posts
* Post.ToString() is overridden to give the wanted view of posts

### Task 3 ###

* Every query is put into task3Queries
* Is made for MsSQL but can be changed for MySQL easily
* Change the hardcoded values if you want to search for different entities

### If I misintepreted something ###

* Please let me know if I can change anything to fit your expectations