﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2WinForms
{
    class Post 
    { 
        private static int _ID = 0;
 
        private int _id;

        public int Id
        {
            get { return _id; }
        }

        private string _author;

        public string Author
        {
            get { return _author; }
          
        }

        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _content;

        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        private DateTime _date;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public Post( string author, string title, string content, DateTime date)
        {
            _id = ++_ID;
            _author = author;
            _title = title;
            _content = content;
            _date = date;
        }
        
        public Post(Post p)
        {
            _id = p.Id;
            _author = p.Author;
            _title = p.Title;
            _content = p.Content;
            _date = new DateTime(p.Date.Year,p.Date.Month,p.Date.Day);
        }

        override
        public string ToString()
        {
            return "Title: " + Title + ";  " +
                    "Author: " + Author + ";  " +
                    "Date: " + Date.ToString() + "\r\n";

        }

        public  static void SortPosts(ref List<Post> posts)
        {
            Post[] postsArray = posts.ToArray();
            for (int i = 0; i < postsArray.Length - 1; i++)
            {
                for (int j = i + 1; j < postsArray.Length; j++)
                {
                    if (postsArray[i].Date < postsArray[j].Date)
                    {
                        Post temp = new Post(postsArray[i]);
                        postsArray[i] = postsArray[j];
                        postsArray[j] = temp;

                    }
                }
            }
            posts = postsArray.ToList();
        }


    }
}
