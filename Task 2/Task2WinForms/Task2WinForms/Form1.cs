﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task2WinForms
{
    public partial class Form1 : Form
    {
        private List<Post> posts;
        public Form1()
        {
            InitializeComponent();
            posts = new List<Post>();
            
            
    }

    private void buttonAddPosts_Click(object sender, EventArgs e)
        {
            if (textBoxNumOfPosts.Equals(string.Empty)) {//if the string is empty, do nothing and warn user to fill this field
                MessageBox.Show("Please input a number of posts you want to add (e. g. 10)");
            }
            else {//We create n posts and add them to our list
                int n = Int32.Parse(textBoxNumOfPosts.Text);
                for (int i = 0; i < n; i++)
                {
                    Post post = new Post("Author_number_ " + i, "Random_title_" + i, "Very_random_content_" + i, RandomDate.getRandomDate()) ;
                    posts.Add(post);
                    listView1.Items.Add(post.ToString());
                }
                Invalidate();
                Refresh();
                    }
        }

        private void buttonSortByDate_Click(object sender, EventArgs e)
        {
            Post.SortPosts(ref posts);
            listView1.Items.Clear();
            foreach (var post in posts)

                listView1.Items.Add(post.ToString());
            Invalidate();


        }

        

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (textBoxNumOfPosts.Equals(string.Empty))
            {//if the string is empty, do nothing and warn user to fill this field
                MessageBox.Show("Please input an ID you want to search)");
            }
            else
            {
                int id = Int32.Parse(textBoxSearchId.Text);
                foreach (var post in posts)
                {
                    if (post.Id == id)
                    {
                        listView1.Items.Clear();
                        listView1.Items.Add(post.ToString());
                        break;
                    }
                }

            }
        }
    }
}
