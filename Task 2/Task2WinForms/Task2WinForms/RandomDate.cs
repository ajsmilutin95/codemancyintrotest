﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2WinForms
{
   public class RandomDate
    {
        private static  DateTime _INITIAL_DATE = new DateTime(2010, 10, 10);// we set the earliest possible date, so we can randomize
        private static Random _RANDOM = new Random();

        public static DateTime getRandomDate()
        {
            DateTime dt = new DateTime(_INITIAL_DATE.Year, _INITIAL_DATE.Month, _INITIAL_DATE.Day);
            int x = _RANDOM.Next();
           return dt.AddDays(x%2000);//%2000 so we wont go too far from the initial date and pass today's date
           
        }
    }
}
