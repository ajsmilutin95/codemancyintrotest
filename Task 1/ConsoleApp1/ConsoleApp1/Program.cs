﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {

        public static int factorial (int n)
        {
            int temp = 1;
            for (int i = 2; i <= n; i++)
                temp *= i;
            return temp;

        }

        public static int factorialRecursive(int n) {
            if (n == 1 || n == 0)
                return 1;
            else 
                return n * factorialRecursive(n - 1);
        }

        static void Main(string[] args)
        {
            int n;
            if (args.Length > 0)
                n = Int32.Parse(args[0]);
            else
            {
                Console.WriteLine("Please enter a number");
                string s=Console.ReadLine();
                n = Int32.Parse(s);
            }
           Console.WriteLine("Factorial: " + factorial(n));
            Console.WriteLine("Factorial Recursive: " + factorialRecursive(n));
        }
    }
}
